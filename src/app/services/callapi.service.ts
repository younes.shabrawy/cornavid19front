import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CallapiService extends DataService {

  constructor(http:HttpClient) {
    super('https://api-coronacovid.herokuapp.com/covid19', http);
   }
}
