import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DeclarationService extends DataService{

  constructor(http:HttpClient) {
    super('https://listedec.herokuapp.com/personne', http);
   }
}
