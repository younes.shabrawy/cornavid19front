import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { CallapiService } from 'src/app/services/callapi.service';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.css']
})
export class DashbordComponent implements OnInit {

  //listeX:any=[];
  //listeY:any=[];
  listeApi:any={
    date:[],
    cases:[]
  };
  //valueX=['01-03','02-03','03-03','04-03','05-03', '06-03'];
  //valueY=[6,13,46,44,120,5];
  highcharts=Highcharts;
  optionCharts:any;
  
  constructor(private callApiService:CallapiService ) { }
  ngOnInit(): void {
    this.callApiService.getAll().subscribe(api =>{
     this.listeApi=api
     console.log(this.listeApi)
     //this.listeY.push(this.listeApi.cases)
    //this.listeX.push(this.listeApi.date)
    
  this.optionCharts = {
      title: {
          text: 'Gestion de Suivi de  Covid-19'
      },
  
      subtitle: {
          text: 'CAS CONFIRMES PAR JOUR'
      },
      chart: {
        type: 'spline',
        backgroundColor: '#EDEDED',
    },
      yAxis: {
          title: {
              text: 'Cas'
          }
      },
      tooltip:{
        valueSuffix:' cas!',
        //valuePrefix:' cas :'
      },

      xAxis: {
        categories: this.listeApi.date
      },
  
      series: [{
          name: 'Cas Confirmé',
          data: this.listeApi.cases,
          color:'#006666'
      }
      
    ],
  
  };
      });
      
      }

}
