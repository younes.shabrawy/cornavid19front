import { DeclarationService } from './../../services/declaration.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-declaration',
  templateUrl: './declaration.component.html',
  styleUrls: ['./declaration.component.css']
})
export class DeclarationComponent implements OnInit {
  status:boolean=false;
  searchPersonne={
    nom:'',
    prenom:'',
    number:'',
    adresse:'',
    description:''
  };
  declaration:any={
    nom:'',
    prenom:'',
    number:'',
    adresse:'',
    description:''
  }
listedeclaration:any=[{
  nom:'',
  prenom:'',
  number:'',
  adresse:'',
  description:''
}]
  constructor(private declarationService: DeclarationService) { }

  ngOnInit(): void {
this.declarationService.getAll().subscribe(res =>{
  this.searchPersonne=this.listedeclaration=res;
  console.log(this.listedeclaration)
})
  }

  ajouterdeclaration(){
this.declarationService.create(this.declaration).subscribe(Response =>{
  this.declaration=Response;
this.listedeclaration.push(this.declaration);
this.declaration={
  nom:'',
  prenom:'',
  number:'',
  adresse:'',
  description:''
}
})
this.status=false
  }

  anulerdeclaration(){
    this.declaration={
      nom:'',
      prenom:'',
      number:'',
      adresse:'',
      description:''
    }
  }
  search(query:string){
    this.searchPersonne = (query) ? this.listedeclaration.filter(personne => personne.nom.toLowerCase().includes(query.toLowerCase()) || personne.prenom.toLowerCase().includes(query.toLowerCase()) ) : this.listedeclaration ;
      }
}
