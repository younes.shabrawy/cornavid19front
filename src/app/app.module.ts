import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HighchartsChartModule } from 'highcharts-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UrgenceComponent } from './components/urgence/urgence.component';
import { DashbordComponent } from './components/dashbord/dashbord.component';
import { DeclarationComponent } from './components/declaration/declaration.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import {HashLocationStrategy , LocationStrategy} from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UrgenceComponent,
    DashbordComponent,
    DeclarationComponent,
    PagenotfoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HighchartsChartModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{provide : LocationStrategy, useClass : HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
