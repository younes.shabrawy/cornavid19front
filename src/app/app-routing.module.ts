import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashbordComponent } from './components/dashbord/dashbord.component';
import { UrgenceComponent } from './components/urgence/urgence.component';
import { DeclarationComponent } from './components/declaration/declaration.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';



const routes: Routes = [
  {path:"",component: DashbordComponent },
  {path:"urgence",component: UrgenceComponent },
  {path:"declaration",component: DeclarationComponent },
  {path:"**",component: PagenotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
